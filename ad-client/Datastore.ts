import {JsonApiDatastoreConfig, JsonApiDatastore, DatastoreConfig, ErrorResponse} from 'angular2-jsonapi';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {User} from './user';
import {AdDate} from './ad-wizard/ad-date.model';
import {Zone} from './ad-wizard/zone.model';
import {Observable, throwError} from 'rxjs';
import {environment} from '../environments/environment';
import {Campaign} from './@core/models/campaign.model';
import {Banner} from './@core/models/banner.model';

const config: DatastoreConfig = {
  baseUrl: environment.apiBaseUrl,
  models: {
    users: User,
    'ad-dates': AdDate,
    zones: Zone,
    campaigns: Campaign,
    banners: Banner,
  },
};

@Injectable()
@JsonApiDatastoreConfig(config)
export class Datastore extends JsonApiDatastore {

  constructor(http: HttpClient) {
    super(http);
  }

  buildCustomUrl(apiPath: string): string {
      const baseUrl = this.datastoreConfig.baseUrl;
      const apiVersion = this.datastoreConfig.apiVersion;

      return [baseUrl, apiVersion, apiPath].filter((x) => x).join('/');
  }

  /**
   * @param error
   * @return {Observable<never>}
   */
  deserializeError(error: any): Observable<never> {
    if (
      error instanceof HttpErrorResponse &&
      error.error instanceof Object &&
      error.error.errors &&
      error.error.errors instanceof Array
    ) {
      const errors: ErrorResponse = new ErrorResponse(error.error.errors);

      return throwError(errors);
    }

    return throwError(error);
  }

}
