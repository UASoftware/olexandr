import { Injectable } from '@angular/core';
import {Datastore} from '../Datastore';
import {AdDate} from './ad-date.model';
import {JsonApiQueryData} from 'angular2-jsonapi';
import * as moment from 'moment';
import {Moment} from 'moment';
import {BehaviorSubject, Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdCalendarService {

  static readonly startDate: Moment = moment.utc();
  static readonly endDate: Moment = moment.utc()
      .add(2, 'months')
      .endOf('month');

  private dates$: BehaviorSubject<{ [key: string]: AdDate; }> = new BehaviorSubject<{ [key: string]: AdDate; }>({});

  constructor(private dataStore: Datastore) { }

    /**
     * Loads a list of ad dates for affiliate and zone id.
     * @param {number} affiliateId
     * @param {number} zoneType
     * @return {Promise<any>}
     */
  loadDates(affiliateId: number, zoneType: number): Promise<any> {
    const params: object = {
        filter: {
            'start-date': this.formatDate(AdCalendarService.startDate),
            'end-date': this.formatDate(AdCalendarService.endDate),
            'zone-type': zoneType,
        },
    };
    const apiUrl: string = this.dataStore.buildCustomUrl(`affiliates/${affiliateId}/ad-dates`);

    return new Promise((resolve, reject) => {
        this.dataStore.findAll(AdDate, params, null, apiUrl).subscribe((data: JsonApiQueryData<AdDate>) => {
          const dateMap: { [key: string]: AdDate; } = this.buildDateMap(data.getModels());
          this.dates$.next(dateMap);
          resolve();
        }, reject);
    });
  }

  /**
   * Searches ad date for a date.
   * @param {Date} date
   * @return {Observable<AdDate>}
   */
  getAdDate(date: Date): Observable<AdDate> {
    const id: string = this.buildDateId(date);

    return this.dates$.pipe(
        map(dates => dates[id]),
    );
  }

  private buildDateId(date: Date): string {
    return this.formatDate(moment(date));
  }

  private formatDate(date: Moment) {
    return date.format('YYYY-MM-DD');
  }

  private buildDateMap(dateList: AdDate[]): { [key: string]: AdDate; } {
    const result: { [key: string]: AdDate; } = {};

    dateList.forEach((adDate: AdDate) => {
      result[adDate.id] = adDate;
    });

    return result;
  }

}
