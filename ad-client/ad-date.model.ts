import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
    type: 'ad-dates',
})
export class AdDate extends JsonApiModel {

  @Attribute({serializedName: 'num-of-purchases'})
  numOfPurchases: number;

  @Attribute({serializedName: 'available-purchases'})
  availablePurchases: number;

  /**
   * Checks whether a date is unavailable.
   * @return {boolean}
   */
  isUnavailable(): boolean {
    return this.availablePurchases === 0;
  }

  /**
   * Checks whether only one purchase is available.
   * @return {boolean}
   */
  isOneLeft(): boolean {
    return this.availablePurchases === 1;
  }

  /**
   * Checks whether more than one day left.
   * @return {boolean}
   */
  isLimited(): boolean {
    return this.availablePurchases > 1 && this.numOfPurchases >= 2;
  }

}
