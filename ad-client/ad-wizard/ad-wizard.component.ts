import {Component, OnInit, ViewChild} from '@angular/core';
import {AdCalendarService} from '../ad-calendar.service';
import {ActivatedRoute} from '@angular/router';
import {Zone} from '../zone.model';
import {AdService} from '../ad.service';
import {NbStepperComponent} from '@nebular/theme';
import {AdOrder} from '../ad-order.model';
import {ErrorResponse} from 'angular2-jsonapi';
import 'rxjs/add/operator/finally';
import {JsonApiError} from 'angular2-jsonapi/dist/models/error-response.model';
import {StepperComponent} from '../stepper/stepper.component';

@Component({
  selector: 'ad-wizard',
  templateUrl: './ad-wizard.component.html',
  styleUrls: ['./ad-wizard.component.scss'],
})
export class AdWizardComponent implements OnInit {

  minDate: Date = AdCalendarService.startDate.local().startOf('day').toDate();
  selectedDates: Date[] = [];
  zone: Zone;
  order: AdOrder;
  isLoading: boolean = false;

  @ViewChild(StepperComponent)
  private stepper: NbStepperComponent;

  /**
   * Index of the payment step
   * @type {number}
   */
  private readonly paymentStepIndex = 2;

  constructor(
    private route: ActivatedRoute,
    private adService: AdService,
    private adCalendarService: AdCalendarService) { }

  ngOnInit() {
    this.zone = this.route.snapshot.data.zone;
  }

  /**
   * Returns true if at least one date is selected.
   * @return {boolean}
   */
  get isDateSelected(): boolean {
    return this.selectedDates.length > 0;
  }

  get isPaymentStep(): boolean {
    return this.stepper.selectedIndex === this.paymentStepIndex;
  }

  onCreateBanner(banner): void {
    this.isLoading = true;
    this.adService.orderAd(this.zone.affiliateId, this.zone.type, banner, this.selectedDates)
      .finally(() => this.isLoading = false)
      .subscribe(
      order => {
        this.order = order;
        this.next();
      },
      (error: ErrorResponse) => this.handleAdOrderingErrors(error),
    );
  }

  next(): void {
    this.stepper.next();
  }

  private handleAdOrderingErrors(errorRes: ErrorResponse): void {
    const datesError = errorRes.errors.find((error: JsonApiError) => {
      return error.meta && error.meta.key && error.meta.key === 'dates';
    });
    if (datesError) {
      this.handleDatesError(datesError);
    } else {
      alert('An error has occurred. Please, try once again.');
    }
  }

  private handleDatesError(error: JsonApiError): void {
    alert(error.detail);
    this.isLoading = true;
    this.adCalendarService.loadDates(this.zone.affiliateId, this.zone.type)
      .then(() => {
        this.isLoading = false;
        this.selectedDates = [];
        this.stepper.previous();
      });
  }

}
