import { Injectable } from '@angular/core';
import {Datastore} from '../Datastore';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import 'rxjs-compat/add/operator/catch';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {

  constructor(private http: HttpClient, private dataStore: Datastore) { }

  /**
   * Creates a new payment for order.
   * @param {string} orderId
   * @return {Observable<string>}
   */
  createPayment(orderId: string): Observable<string> {
    const url: string = this.dataStore.buildCustomUrl(`ad-orders/${orderId}/payments`);

    return this.http.post<string>(url, {}).pipe(
      map((res: any) => {
        return res.data.id;
      }),
    ).catch((res: HttpErrorResponse) => this.dataStore.deserializeError(res));
  }

  /**
   * @param {string} orderId
   * @param {string} payerId
   * @return {Observable<Object>}
   */
  executePayment(orderId: string, payerId: string): Observable<Object> {
    const url: string = this.dataStore.buildCustomUrl(`ad-orders/${orderId}/payments/execute`);
    const data: object = {
      'payer-id': payerId,
    };

    return this.http.post(url, data)
      .catch((res: HttpErrorResponse) => this.dataStore.deserializeError(res));
  }

}
