import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdOrder} from '../ad-order.model';
import {PaymentService} from '../payment.service';
import {environment} from '../../../environments/environment';
import {PayPalConfig, PayPalIntegrationType} from 'ngx-paypal';
import {Observable, throwError} from 'rxjs';
import {IPayPalPaymentCompleteData} from 'ngx-paypal/lib/models/paypal-models';
import {catchError, map} from 'rxjs/operators';
import {ErrorResponse} from 'angular2-jsonapi';

@Component({
  selector: 'ad-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {

  @Input() order: AdOrder;
  @Output() completed = new EventEmitter<boolean>();
  payPalConfig?: PayPalConfig;

  constructor(private paymentService: PaymentService) { }

  ngOnInit() {
    this.initPaypal();
  }

  private initPaypal(): void {
    const config: object = {
      button: {
        size: 'medium',
      },
      payment: () => this.pay(),
      onAuthorize: (data, actions) => this.onAuthorize(data, actions),
      onError: () => alert('An error has occurred during the payment. Please, try again.'),
    };

    this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ServerSideREST, environment.payPalEnvironment, config);
  }

  private pay(): Observable<string> {
    return this.paymentService.createPayment(this.order.id);
  }

  private onAuthorize(data: IPayPalPaymentCompleteData, actions: any): Observable<void> {
    return this.paymentService.executePayment(this.order.id, data.payerID).pipe(
      map(() => {
        this.completed.emit(true);
        return undefined;
      }),
      catchError(error => {
        if (error instanceof ErrorResponse && error.errors[0].detail === 'INSTRUMENT_DECLINED') {
          return actions.restart();
        }

        return throwError(error);
      }),
    );

  }

}
