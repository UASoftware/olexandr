import {Component, Inject, OnInit} from '@angular/core';
import {NB_AUTH_OPTIONS} from '@nebular/auth';
import {getDeepFromObject} from '@nebular/auth/helpers';
import {UserService} from '../../user.service';
import {ErrorResponse} from 'angular2-jsonapi';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {

  submitted = false;
  user: any = {};
  errorText = '';
  successText = '';
  deactivateLoading = false;
  saveDataLoading = false;

  constructor(private userService: UserService,
              private router: Router,
              @Inject(NB_AUTH_OPTIONS) protected options = {}) {
  }

  saveData(): void {
    this.saveDataLoading = true;
    if (this.user.password !== undefined && !this.user.password ) {
      this.user.password = undefined;
    }
    this.userService.updateUserInfo(this.user).subscribe(null, err => {
      if (err instanceof ErrorResponse && err.errors && err.errors.length > 0) {
        this.errorText = err.errors[0].detail;
        this.saveDataLoading = false;
      }
    },  () => {
      this.errorText = '';
      this.successText = 'Your username/password has been updated';
      this.saveDataLoading = false;
    } );
  }

  deactivateAccount() {
    if (confirm('Are you sure want to deactivate your account? \nThis action can\'t be undone.') !== false) {
      this.deactivateLoading = true;
      this.user.password = undefined;
      this.userService.deactivateCurrentUser(this.user).subscribe(null, () => {}, () => {
        this.deactivateLoading = false;
        this.router.navigateByUrl('/auth/logout');
      });
    }
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  onErrorClose() {
    this.errorText = '';
  }

  onSuccessClose() {
    this.successText = '';
  }

  ngOnInit() {
    this.userService.getUser().subscribe(user => {
      this.user = user;
    })
  }
}
