import { Component, OnInit } from '@angular/core';
import {AdOrderService} from '../../../@core/ad-order.service';
import {UserService} from '../../../user.service';
import {LocalDataSource} from 'ng2-smart-table';
import {AdOrder} from '../../../ad-wizard/ad-order.model';
import * as moment from 'moment';
import {Campaign} from '../../../@core/models/campaign.model';
import {BannerViewCellComponent} from '../banner-view-cell/banner-view-cell.component';

@Component({
  selector: 'ngx-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss'],
})
export class PurchaseHistoryComponent implements OnInit {

  settings = {
    hideSubHeader: true,
    columns: {
      id: {
        title: 'Payment ID',
        filter: false,
      },
      price: {
        title: 'Price',
        filter: false,
      },
      state: {
        title: 'State',
        filter: false,
      },
      date: {
        title: 'Purchase Date',
        filter: false,
      },
      adDates: {
        title: 'Ad Dates',
        filter: false,
        sort: false,
      },
      ad: {
        title: 'Ad',
        filter: false,
        type: 'custom',
        sort: false,
        renderComponent: BannerViewCellComponent,
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
  };

  source: LocalDataSource;
  isLoading: boolean = false;

  constructor(private adOrderService: AdOrderService, private userService: UserService) { }

  ngOnInit() {
    this.isLoading = true;
    this.source = new LocalDataSource();
    this.adOrderService.loadUserAdOrders(this.userService.getUserId()).subscribe((orders: AdOrder[]) => {
      this.source.load(this.buildTableData(orders));
      this.isLoading = false;
    });
  }

  private buildTableData(orderList: AdOrder[]): object[] {
    const result = [];

    orderList.forEach((order: AdOrder) => {
      const campaign: Campaign = order.campaigns[0];
      let adDates: string[] = [];

      order.campaigns.forEach((camp: Campaign) => {
        adDates = adDates.concat(camp.dates);
      });

      const record: object = {
        id: order.paymentId,
        price: order.price,
        state: order.paymentState,
        date: moment(order.updatedAt).format('LLL'),
        adDates: adDates.length ? adDates.join(', ') : 'N/A',
        ad: (campaign && campaign.banners[0]) ? campaign.banners[0] : null,
      };
      result.push(record);
    });

    return result;
  }

}
