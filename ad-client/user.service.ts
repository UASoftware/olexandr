import { Injectable } from '@angular/core';
import {User} from './user';
import { Observable } from 'rxjs';
import {Datastore} from './Datastore';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  private currentId: string;

  constructor(private datastore: Datastore,
              private nbTokenService: NbTokenService) {
    this.nbTokenService.tokenChange().subscribe((token: NbAuthJWTToken) => {
      this.currentId = token.isValid() ? token.getPayload().sub : null;
    })
  }

  updateUserInfo(user: User): Observable<any> {
    return user.save();
  }

  deactivateCurrentUser(user: User): Observable<any> {
    user.active = false;
    return user.save();
  }

  getUser(): Observable<User> {
    return this.datastore.findRecord(User, this.currentId);
  }

  getUserId(): string {
    return this.currentId;
  }

}
