import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'users',
})
export class User extends JsonApiModel {
  @Attribute()
  username: string;
  @Attribute()
  password: string;
  @Attribute()
  active: boolean;
}
