import { Injectable } from '@angular/core';
import {ZoneService} from './zone.service';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Zone} from './zone.model';
import {EMPTY, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ZoneResolverService implements Resolve<Zone> {

  constructor(private zoneService: ZoneService, private router: Router) { }

  /**
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @return {Observable<Zone>}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Zone> {
    const affiliateId: number = parseInt(route.paramMap.get('affiliateId'), 10);
    const zoneType: number = parseInt(route.paramMap.get('zoneType'), 10);

    return this.zoneService.getZoneOfType(affiliateId, zoneType).catch(() => {
      this.router.navigate(['miscellaneous/404']);
      return EMPTY;
    });
  }

}
