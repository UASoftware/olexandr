import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
    type: 'zones',
})
export class Zone extends JsonApiModel {

  /**
   * Banner zone type
   * @type {number}
   */
  static readonly bannerZone: number = 0;

  /**
   * Banner zone type
   * @type {number}
   */
  static readonly textZone: number = 3;

  @Attribute()
  name: string;

  @Attribute({serializedName: 'zone-type'})
  type: number;

  @Attribute()
  width: number;

  @Attribute()
  height: number;

  @Attribute({serializedName: 'affiliate-id'})
  affiliateId: number;

  get isBannerZone(): boolean {
    return this.type === Zone.bannerZone;
  }

  get isTextZone(): boolean {
    return this.type === Zone.textZone;
  }

}
