import { Injectable } from '@angular/core';
import {Datastore} from '../Datastore';
import {JsonApiQueryData} from 'angular2-jsonapi';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Zone} from './zone.model';

@Injectable({
  providedIn: 'root',
})
export class ZoneService {

  constructor(private dataStore: Datastore) { }

  /**
   * Loads a list of zones for affiliate.
   * @param {number} affiliateId
   * @param {number} zoneType
   * @return {Observable<Zone[]>}
   */
  loadZones(affiliateId: number, zoneType: number): Observable<Zone[]> {
    const params: object = {
      filter: {
        'zone-type': zoneType,
      },
    };
    const apiUrl: string = this.dataStore.buildCustomUrl(`affiliates/${affiliateId}/zones`);

    return this.dataStore.findAll(Zone, params, null, apiUrl).pipe(
      map((data: JsonApiQueryData<Zone>) => data.getModels()),
    );
  }

  /**
   * Returns a first zone of type.
   * @param {number} affiliateId
   * @param {number} zoneType
   * @return {Observable<Zone>}
   */
  getZoneOfType(affiliateId: number, zoneType: number): Observable<Zone> {
    return this.loadZones(affiliateId, zoneType).pipe(
      map((zones: Zone[]) => {
        if (zones[0]) {
          return zones[0];
        }

        throw new Error(`The affiliate with id ${affiliateId} doesn't have zones of type ${zoneType}.`);
      }),
    );
  }

}
