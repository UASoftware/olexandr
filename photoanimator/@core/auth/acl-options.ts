/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */
import { NbAclOptions } from '@nebular/security';
import { AclResource } from './acl-resource.enum';

export const aclOptions: NbAclOptions = <NbAclOptions>{
  accessControl: {
    user: {
      view: AclResource.SubUsers,
      create: AclResource.SubUsers,
      edit: AclResource.SubUsers,
      remove: AclResource.SubUsers,
    },
    admin: {
      view: AclResource.All,
      create: AclResource.All,
      edit: AclResource.All,
      remove: AclResource.All,
    },
  },
};
