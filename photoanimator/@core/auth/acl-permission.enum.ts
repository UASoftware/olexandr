/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

export enum AclPermission {

  View = 'view',
  Create = 'create',
  Edit = 'edit',
  Delete = 'delete',

}
