/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

export enum AclResource {

  SubUsers = 'sub_users',
  All = '*',

}
