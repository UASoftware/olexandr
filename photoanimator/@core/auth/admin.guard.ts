/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { NbRoleProvider } from '@nebular/security';
import { Role } from './role.enum';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {

  constructor(private roleProvider: NbRoleProvider) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.roleProvider.getRole()
      .pipe(
        // RoleProvider.getRole() can return a single role or an array.
        // In case of single role convert it to array for further processing.
        map((roleOrRoles: string | string[]) => {
          return Array.isArray(roleOrRoles) ? roleOrRoles : [roleOrRoles];
        }),
        map((roles: string[]) => {
          return (!!~roles.indexOf(Role.Admin));
        }),
    );
  }

}
