/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */
import { Injectable } from '@angular/core';
import { NbAuthResult, NbAuthStrategy, NbAuthStrategyClass, NbPasswordAuthStrategyOptions } from '@nebular/auth';
import { Observable, of } from 'rxjs';
import { AuthService } from '../graphql/auth.service';
import { map, catchError } from 'rxjs/operators';
import { GraphqlErrorExtractorService } from '../graphql/graphql-error-extractor.service';

@Injectable()
export class AuthStrategy extends NbAuthStrategy {

  static setup(options: NbPasswordAuthStrategyOptions): [NbAuthStrategyClass, NbPasswordAuthStrategyOptions] {
    return [AuthStrategy, options];
  }

  constructor(private authService: AuthService, private errorExtractor: GraphqlErrorExtractorService) {
    super();
  }

  authenticate(data?: any): Observable<NbAuthResult> {
    const module = 'login';
    return this.authService.login(data.username, data.password).pipe(
      map(token  => {
        return new NbAuthResult(
          true,
          token,
          this.getOption(`${module}.redirect.success`),
          [],
          [],
          this.createToken(token, true),
        );
      }),
      catchError(error => {
        return this.handleResponseError(error, module);
      }),
    );
  }

  register(data?: any): Observable<NbAuthResult> {
    throw new Error('register is not implemented');
  }

  requestPassword(data?: any): Observable<NbAuthResult> {
    throw new Error('requestPassword is not implemented');
  }

  resetPassword(data?: any): Observable<NbAuthResult> {
    throw new Error('resetPassword is not implemented');
  }

  logout(): Observable<NbAuthResult> {
    const module = 'logout';
    return this.authService.logout().pipe(
      map(res => {
        return this.createAuthResult(res, true, `${module}.redirect.success`, []);
      }),
      catchError((res) => {
        return of(
          this.createAuthResult(
            res,
            true, // it's true because in order to remove the stored token we need to return a success result
            `${module}.redirect.failure`,
            []),
        );
      }),
    );
  }

  refreshToken(data?: any): Observable<NbAuthResult> {
    throw new Error('refreshToken is not implemented');
  }

  /**
   * Handles graphql errors.
   * @param error
   * @param {string} module
   * @return {Observable<NbAuthResult>}
   */
  protected handleResponseError(error: any, module: string): Observable<NbAuthResult> {
    return of(
      this.createAuthResult(
        error,
        false,
        `${module}.redirect.failure`,
        this.errorExtractor.extractGraphqlErrors(error),
      ),
    );
  }

  /**
   * Creates an instance of NbAuthResult.
   * @param result
   * @param {boolean} isSuccess
   * @param {string} redirectKey
   * @param {string[]} errors
   * @return {NbAuthResult}
   */
  protected createAuthResult(result: any, isSuccess: boolean, redirectKey: string, errors: string[]): NbAuthResult {
    return new NbAuthResult(
      isSuccess,
      result,
      this.getOption(redirectKey),
      errors,
    );
  }

}
