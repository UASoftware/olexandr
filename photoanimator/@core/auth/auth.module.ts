/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthJWTToken, NbAuthModule } from '@nebular/auth';
import { AuthStrategy } from './auth-strategy';

const authStrategy = 'graphql';
const authOptions = {
  strategies: [
    AuthStrategy.setup({
      name: authStrategy,
      token: {
        class: NbAuthJWTToken,
      },
      login: {
        redirect: {
          success: '/',
        },
      },
      logout: {
        redirect: {
          success: '/auth/login',
          failure: '/auth/login',
        },
      },
    }),
  ],
  forms: {
    login: {
      redirectDelay: 0, // delay before redirect after a successful login, while success message is shown to the user
      strategy: authStrategy,  // strategy id key.
      rememberMe: false,   // whether to show or not the `rememberMe` checkbox
      showMessages: {     // show/not show success/error messages
        success: false,
        error: true,
      },
    },
    logout: {
      strategy: authStrategy,
    },
    validation: {
      username: {
        required: true,
      },
      password: {
        minLength: 6,
      },
    },
  },
};

@NgModule({
  declarations: [],
  providers: [AuthStrategy],
  imports: [
    CommonModule,
    NbAuthModule.forRoot(authOptions),
  ],
})
export class AuthModule { }
