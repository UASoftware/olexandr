/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

export enum Role {
  Admin = 'admin',
  User = 'user',
}
