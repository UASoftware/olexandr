/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */
import { Injectable } from '@angular/core';
import { NbRoleProvider } from '@nebular/security';
import { Observable } from 'rxjs';
import { NbAuthService, NbAuthToken } from '@nebular/auth';
import { map } from 'rxjs/internal/operators';
import { Role } from './role.enum';

@Injectable()
export class RoleProvider extends NbRoleProvider {

  /**
   * The key of the roles array in the token payload.
   * @type {string}
   */
  private static readonly rolesKey = 'roles';

  constructor(private nbAuthService: NbAuthService) {
    super();
  }

  /**
   * Returns a list of roles from the auth token payload or default role.
   * @return {Observable<string | string[]>}
   */
  getRole(): Observable<string | string[]> {
    return this.nbAuthService.onTokenChange().pipe(
      map((token: NbAuthToken) => {
        return token.isValid() ? token.getPayload()[RoleProvider.rolesKey] : Role.User;
      }),
    );
  }

}
