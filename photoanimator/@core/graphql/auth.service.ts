/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { loginMutation, LoginMutationResponse, logoutMutation } from './schema';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private apollo: Apollo) { }

  /**
   * Logs in user with username and password and returns a JWT.
   * @param {string} username
   * @param {string} password
   * @return {Observable<string>}
   */
  login(username: string, password: string): Observable<string> {
    this.apollo.getClient().clearStore();
    return this.apollo.mutate<LoginMutationResponse>({
      mutation: loginMutation,
      variables: {
        input: {
          username: username,
          password: password,
        },
      },
    }).pipe(
      map(response => {
        return response.data.login;
      }),
    );
  }

  /**
   * Logs out user.
   * @return {Observable<void>}
   */
  logout(): Observable<void> {
    this.apollo.getClient().clearStore();
    return this.apollo.mutate({
      mutation: logoutMutation,
    }).pipe(
      map(() => {
        return undefined;
      }),
    );
  }

}
