/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */


import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GraphqlErrorExtractorService {

  private static readonly defaultError: string = 'Something went wrong, please try again.';
  private static readonly validationCategory: string = 'validation';

  constructor() { }

  /**
   * Extracts errors from graphql response.
   * @param error
   * @return {string[]}
   */
  public extractGraphqlErrors(error: any): string[] {
    const errors = [];

    if (this.hasGraphQLErrors(error)) {
      errors.push(error.graphQLErrors[0].message);
    } else {
      errors.push(GraphqlErrorExtractorService.defaultError);
    }

    return errors;
  }

  /**
   * Returns a list of validation errors.
   * @param error
   * @return {string[]}
   */
  public extractValidationErrors(error: any): string[] {
    let errors = [];

    if (!this.hasValidationErrors(error)) {
      errors.push(GraphqlErrorExtractorService.defaultError);
      return errors;
    }

    error.graphQLErrors.forEach(graphqlError => {
      Object.keys(graphqlError.extensions.validation).forEach(key => {
        errors = errors.concat(graphqlError.extensions.validation[key]);
      });
    });

    return errors;
  }

  /**
   * Returns true if the response has graphql errors.
   * @param error
   * @return {boolean}
   */
  public hasGraphQLErrors(error: any): boolean {
    return error.graphQLErrors && error.graphQLErrors.length > 0;
  }

  /**
   * Returns true if the response has validation errors.
   * @param error
   * @return {boolean}
   */
  public hasValidationErrors(error: any): boolean {
    if (this.hasGraphQLErrors(error)) {
      return error.graphQLErrors.some(graphqlError => {
        return graphqlError.extensions.category === GraphqlErrorExtractorService.validationCategory;
      });
    }

    return false;
  }

}
