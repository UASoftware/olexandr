/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { MutationOptions, OperationVariables } from 'apollo-client';
import { RefetchQueryDescription } from 'apollo-client/core/watchQueryOptions';

export class MutationOptionsFactory {

  /**
   * Creates MutationOptions with awaitRefetchQueries set to true.
   * @param {DocumentNode} mutation
   * @param {OperationVariables} variables
   * @param {RefetchQueryDescription} refetchQueries
   * @return {MutationOptions}
   */
  public createOptions(
    mutation: any,
    variables?: OperationVariables,
    refetchQueries?: RefetchQueryDescription,
  ): MutationOptions {
    return <MutationOptions> {
      mutation: mutation,
      variables: variables,
      refetchQueries: refetchQueries,
      awaitRefetchQueries: true,
    };
  }

}
