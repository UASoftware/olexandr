/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { onError } from 'apollo-link-error';

const noConnectionMessage = 'No Internet Connection.\nCheck your connection and refresh the page to re-sync state.';
const unsuccessfulResponseMessage = 'The server has returned an unsuccessful response code. Refresh the page to re-sync state.';

const networkErrorHandler = onError(({ networkError }) => {
  if (networkError) {
    if (!navigator.onLine) {
      alert(noConnectionMessage);
    } else {
      alert(unsuccessfulResponseMessage);
    }
  }
});

export default networkErrorHandler;
