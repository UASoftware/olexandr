/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import gql from 'graphql-tag';
import { User } from '../models/user.model';

export const loginMutation = gql`
    mutation LoginMutation($input: LoginInput!) {
        login(input: $input)
    }
`;

export interface LoginMutationResponse {
    login: string;
}

export const logoutMutation = gql`
    mutation LogoutMutation {
        logout
    }
`;

export const subusersQuery = gql`
  query {
      me {
          subusers {
              id
              username
              name
              email
          }
      }
  }
`;

export interface SubusersQueryResponse {
  me: {
    subusers: User[],
  };
}

export const deleteSubuserMutation = gql`
  mutation deleteSubuserMutation($id: Int!) {
      deleteSubuser(id: $id) {
          id
          username
          name
          email
      }
  }
`;

export interface DeleteSubuserMutationResponse {
  deleteSubuser: User;
}

const UserPropertiesFragment = gql`
    fragment UserProperties on User {
        id
        username
        name
        email
    }
`;

export const createSubuserMutation = gql`
    mutation createSubuserMutation($input: CreateSubuserInput!) {
        createSubuser(input: $input) {
            ...UserProperties
        }
    }
    ${UserPropertiesFragment}
`;

export interface CreateSubuserMutationResponse {
  createSubuser: User;
}

export const updateSubuserMutation = gql`
    mutation updateSubuserMutation($input: UpdateSubuserInput!) {
        updateSubuser(input: $input) {
            ...UserProperties
        }
    }
    ${UserPropertiesFragment}
`;

export interface UpdateSubuserMutationResponse {
  updateSubuser: User;
}

export const numOfAvailableSubusersQuery = gql`
  query {
      numOfAvailableSubusers
  }
`;

export interface NumOfAvailableSubusersResponse {
  numOfAvailableSubusers: number;
}
