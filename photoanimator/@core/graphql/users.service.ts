/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MutationOptions } from 'apollo-client';
import { User } from '../models/user.model';
import {
  deleteSubuserMutation,
  DeleteSubuserMutationResponse,
  subusersQuery,
  SubusersQueryResponse,
  createSubuserMutation,
  CreateSubuserMutationResponse,
  updateSubuserMutation,
  UpdateSubuserMutationResponse,
  NumOfAvailableSubusersResponse,
  numOfAvailableSubusersQuery,
} from './schema';
import { MutationOptionsFactory } from './mutation-options.factory';
import { UserInput } from '../models/user-input.model';

const refetchSubusersQueryDescription = [{
  query: subusersQuery,
}];
// refetch sub users and number of available sub users
const refetchSubusersDataQueryDesc = [{
  query: numOfAvailableSubusersQuery,
}].concat(refetchSubusersQueryDescription);

@Injectable({
  providedIn: 'root',
})
export class UsersService {

  constructor(private apollo: Apollo, private mutationOptionsFactory: MutationOptionsFactory) { }

  /**
   * Returns a list of sub users of the authenticated user.
   * @return {Observable<User[]>}
   */
  public getSubUsers(): Observable<User[]> {
    return this.apollo
      .watchQuery<SubusersQueryResponse>({
        query: subusersQuery,
      })
      .valueChanges.pipe(
        map(response => response.data.me.subusers),
      );
  }

  /**
   * Deletes a sub user.
   * @param {number} userId
   * @return {Observable<User>}
   */
  public deleteSubUser(userId: number): Observable<User> {
    const options: MutationOptions = this.mutationOptionsFactory.createOptions(
      deleteSubuserMutation,
      {id: userId},
      refetchSubusersDataQueryDesc,
    );

    return this.apollo
      .mutate<DeleteSubuserMutationResponse>(options)
      .pipe(map(response => response.data.deleteSubuser));
  }

  /**
   * Creates a sub user.
   * @param {UserInput} userInput
   * @return {Observable<User>}
   */
  public createSubUser(userInput: UserInput): Observable<User> {
    const options: MutationOptions = this.mutationOptionsFactory.createOptions(
      createSubuserMutation,
      {input: userInput},
      refetchSubusersDataQueryDesc,
    );

    return this.apollo.mutate<CreateSubuserMutationResponse>(options)
      .pipe(map(response => response.data.createSubuser));
  }

  /**
   * Updates a sub user.
   * @param {UserInput} userInput
   * @return {Observable<User>}
   */
  public updateSubUser(userInput: UserInput): Observable<User> {
    const options: MutationOptions = this.mutationOptionsFactory.createOptions(
      updateSubuserMutation,
      {input: userInput},
      refetchSubusersQueryDescription,
    );

    return this.apollo.mutate<UpdateSubuserMutationResponse>(options)
      .pipe(map(response => response.data.updateSubuser));
  }

  /**
   * Returns a number of available sub users.
   * @return {Observable<number>}
   */
  public getNumOfAvailableSubUsers(): Observable<number> {
    return this.apollo
      .watchQuery<NumOfAvailableSubusersResponse>({
        query: numOfAvailableSubusersQuery,
      })
      .valueChanges.pipe(
        map(response => response.data.numOfAvailableSubusers),
      );
  }

}
