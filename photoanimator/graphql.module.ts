/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { environment } from '../environments/environment';
import { NbAuthService, NbAuthToken, NbTokenService } from '@nebular/auth';
import { Router } from '@angular/router';
import { ApolloLink } from 'apollo-link';
import { MutationOptionsFactory } from './@core/graphql/mutation-options.factory';
import networkErrorHandler from './@core/graphql/network-error-handler';

const uri = environment.backendEndpoint; // <-- add the URL of the GraphQL server here
export function createApollo(httpLink: HttpLink, authService: NbAuthService, router: Router, tokenService: NbTokenService) {
  const http = httpLink.create({uri});
  // Set authorization header
  const auth = setContext(async(ctx, { headers }) => {
    const token: NbAuthToken = await authService.getToken().toPromise();

    // NOTE: the line new HttpHeaders().set('Authorization', `Bearer ${token.getValue()}`) is not working with createUploadLink
    return {
      headers: {Authorization: `Bearer ${token.getValue()}`},
    };
  });
  // Handle auth errors
  const authErrorHandler = onError(({ graphQLErrors }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ extensions }) => {
        if (extensions.category === 'authentication') {
          tokenService.clear();
          router.navigate(['auth/login']);
        }
      });
    }
  });

  return {
    link: ApolloLink.from([auth, networkErrorHandler, authErrorHandler, http]),
    cache: new InMemoryCache(),
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, NbAuthService, Router, NbTokenService],
    },
    MutationOptionsFactory,
  ],
})
export class GraphQLModule {}
