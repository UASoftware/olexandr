/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/finally';
import { User } from '../../../@core/models/user.model';
import { GraphqlErrorExtractorService } from '../../../@core/graphql/graphql-error-extractor.service';
import { UsersService } from '../../../@core/graphql/users.service';
import { UserInput } from '../../../@core/models/user-input.model';

@Component({
  templateUrl: './sub-user-editor.component.html',
})
export class SubUserEditorComponent {

  @Input() title: string;
  @Input() user: User;
  errors: string[] = [];
  isLoading: boolean = false;

  constructor(
    private dialogRef: NbDialogRef<SubUserEditorComponent>,
    private errorExtractor: GraphqlErrorExtractorService,
    private usersService: UsersService,
  ) { }

  get passwordRequired(): boolean {
    return !this.user.id;
  }

  /**
   * Closes the dialog window.
   */
  closeDialog(): void {
    this.dialogRef.close(false);
  }

  /**
   * Saves the sub user and extracts errors for displaying.
   * @param {UserInput} userInput
   */
  saveUser(userInput: UserInput): void {
    this.isLoading = true;
    this.createOrUpdateUser(userInput)
      .finally(() => this.isLoading = false)
      .subscribe(
        () => this.dialogRef.close(true),
        (error) => this.errors = this.errorExtractor.extractValidationErrors(error),
      );
  }

  /**
   * Creates or updates the sub user.
   * @param {UserInput} userInput
   * @return {Observable<User>}
   */
  private createOrUpdateUser(userInput: UserInput): Observable<User> {
    if (this.user.id) {
      userInput.id = this.user.id;
      return this.usersService.updateSubUser(userInput);
    }

    return this.usersService.createSubUser(userInput);
  }

}
