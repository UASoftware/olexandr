/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { User } from '../../../@core/models/user.model';
import { UserInput } from '../../../@core/models/user-input.model';

const maxStringLength = 255;
const minPasswordLength = 6;
const usernamePattern = /^[-a-z0-9_]+$/i;
const namePattern = /^[a-z][^#&<>\\"~;$^%{}?]+$/i;

@Component({
  selector: 'ngx-sub-user-form',
  templateUrl: './sub-user-form.component.html',
})
export class SubUserFormComponent implements OnInit {

  @Input() user: User;
  @Input() passwordRequired: boolean;
  @Output() save = new EventEmitter<UserInput>();
  @Output() cancel = new EventEmitter<void>();


  form: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.pattern(usernamePattern), Validators.maxLength(maxStringLength)]],
    password: [''],
    email: ['', [Validators.required, Validators.email, Validators.maxLength(maxStringLength)]],
    name: ['', [Validators.required, Validators.maxLength(maxStringLength), Validators.pattern(namePattern)]],
  });

  constructor(private fb: FormBuilder) { }

  get usernameControl(): AbstractControl {
    return this.form.get('username');
  }

  get passwordControl(): AbstractControl {
    return this.form.get('password');
  }

  get emailControl(): AbstractControl {
    return this.form.get('email');
  }

  get nameControl(): AbstractControl {
    return this.form.get('name');
  }

  private get passwordValidators(): ValidatorFn[] {
    const validators: ValidatorFn[] = [
      Validators.minLength(minPasswordLength),
      Validators.maxLength(maxStringLength),
    ];

    if (this.passwordRequired) {
      validators.unshift(Validators.required);
    }

    return validators;
  }

  ngOnInit() {
    this.form.setValue({
      username: this.user.username,
      password: '',
      email: this.user.email,
      name: this.user.name,
    });
    this.passwordControl.setValidators(this.passwordValidators);
  }

  cancelForm(): void {
    this.cancel.emit();
  }

  submitForm(): void {
    const userInput: UserInput = <UserInput>{
      username: this.usernameControl.value,
      email: this.emailControl.value,
      name: this.nameControl.value,
    };
    if (this.passwordControl.value) {
      userInput.password = this.passwordControl.value;
    }
    this.save.emit(userInput);
  }

}
