/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Component, OnInit } from '@angular/core';
import { NbDialogConfig, NbDialogService } from '@nebular/theme';
import { DataTableComponent } from '../../../@theme/components/data-table/data-table.component';
import { UsersService } from '../../../@core/graphql/users.service';
import { User } from '../../../@core/models/user.model';
import { SubUserEditorComponent } from '../sub-user-editor/sub-user-editor.component';

const tableTitle = 'Sub Users';
const addButtonTitle = 'Add Sub User';
const tableColumns = {
  id: {
    title: 'ID',
  },
  username: {
    title: 'Username',
  },
  name: {
    title: 'Name',
  },
  email: {
    title: 'Email',
  },
};
const createDialogTitle = 'Add Sub User';
const editDialogTitle = 'Edit Sub User';
const numOfUsersLeftGenerator = (num) => `${num} left`;


@Component({
  templateUrl: './sub-user-list.component.html',
  styleUrls: ['../../../@theme/components/data-table/data-table.component.scss'],
})
export class SubUserListComponent extends DataTableComponent implements OnInit {

  numOfAvailableSubUsers: number;

  constructor(private usersService: UsersService, private dialogService: NbDialogService) {
    super();
  }

  get isAddBtnDisabled(): boolean {
    return this.numOfAvailableSubUsers === undefined || this.numOfAvailableSubUsers === 0;
  }

  get addBtnTooltip(): string {
    return numOfUsersLeftGenerator(this.numOfAvailableSubUsers);
  }

  ngOnInit() {
    super.ngOnInit();
    this.title = tableTitle;
    this.addButtonTitle = addButtonTitle;
    this.settings.columns = tableColumns;
    this.loadNumOfAvailableSubUsers();
    this.loadSubUsers();
  }

  delete(event: any) {
    const user: User = this.eventDataToModel<User>(event);

    if (confirm(`Are you sure want to delete "${user.username}" user?`)) {
      this.isLoading = true;
      this.usersService.deleteSubUser(user.id)
        .finally(() => this.isLoading = false)
        .subscribe();
    }
  }

  create(): void {
    const user: User = <User>{
      id: null,
      username: '',
      name: '',
      email: '',
    };
    const config: Partial<NbDialogConfig<Partial<SubUserEditorComponent>>> = <Partial<NbDialogConfig<Partial<SubUserEditorComponent>>>>{
      context: {
        title: createDialogTitle,
        user: user,
      },
    };
    this.dialogService.open(SubUserEditorComponent, config);
  }

  edit(event: Object): void {
    const config: Partial<NbDialogConfig<Partial<SubUserEditorComponent>>> = <Partial<NbDialogConfig<Partial<SubUserEditorComponent>>>>{
      context: {
        title: editDialogTitle,
        user: this.eventDataToModel<User>(event),
      },
    };
    this.dialogService.open(SubUserEditorComponent, config);
  }

  /**
   * Loads a list of sub users.
   */
  private loadSubUsers(): void {
    this.isLoading = true;
    this.usersService.getSubUsers().subscribe((users: User[]) => {
      this.source.load(users);
      this.isLoading = false;
    });
  }

  /**
   * Loads number of available sub users.
   */
  private loadNumOfAvailableSubUsers(): void {
    this.usersService.getNumOfAvailableSubUsers()
      .subscribe((numOfAvailableSubUsers: number) => this.numOfAvailableSubUsers = numOfAvailableSubUsers);
  }

}
