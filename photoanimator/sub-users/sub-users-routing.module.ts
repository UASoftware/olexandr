/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubUserListComponent } from './sub-user-list/sub-user-list.component';

const routes: Routes = [
  {
    path: '',
    component: SubUserListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})

export class SubUsersRoutingModule { }
