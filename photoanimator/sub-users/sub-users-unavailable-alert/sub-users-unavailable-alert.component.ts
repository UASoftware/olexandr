/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-sub-users-unavailable-alert',
  templateUrl: './sub-users-unavailable-alert.component.html',
})
export class SubUsersUnavailableAlertComponent {

  @Input() numOfAvailableSubUsers: number;

  get shouldDisplayAlert(): boolean {
    return this.numOfAvailableSubUsers === 0;
  }

}
