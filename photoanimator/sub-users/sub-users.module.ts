/**
 * This source file is part of PhotoAnimator.
 * Copyright (c) 2019.
 * All rights reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbDialogModule, NbTooltipModule } from '@nebular/theme';
import { SubUsersRoutingModule } from './sub-users-routing.module';
import { SubUserListComponent } from './sub-user-list/sub-user-list.component';
import { ThemeModule } from '../../@theme/theme.module';
import { SubUserEditorComponent } from './sub-user-editor/sub-user-editor.component';
import { SubUserFormComponent } from './sub-user-form/sub-user-form.component';
import { SubUsersUnavailableAlertComponent } from './sub-users-unavailable-alert/sub-users-unavailable-alert.component';

@NgModule({
  declarations: [
    SubUserListComponent,
    SubUserEditorComponent,
    SubUserFormComponent,
    SubUsersUnavailableAlertComponent,
  ],
  imports: [
    ThemeModule,
    CommonModule,
    Ng2SmartTableModule,
    NbDialogModule.forChild(),
    SubUsersRoutingModule,
    NbTooltipModule,
  ],
  entryComponents: [
    SubUserEditorComponent,
  ],
})
export class SubUsersModule { }
